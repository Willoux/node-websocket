const http = require('http');

const server = http.createServer();

const io = require('socket.io')(server)

const pino = require('pino')('app.log');


server.on('request', (request, response) => {
    if (request.url == '/') {
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.write('ok')
        response.end()
    }
});


io.on('connect', (socket) =>{
    pino.info('websocket connected')
});


server.listen();

pino.info('App started');